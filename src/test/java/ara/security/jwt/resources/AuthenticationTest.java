package ara.security.jwt.resources;

import ara.security.jwt.exceptions.security.AuthException;
import ara.security.jwt.model.security.JwtAuthenticationRequest;
import ara.security.jwt.service.security.JwtTokenService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthenticationTest {

    private MockMvc mvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private JwtTokenService jwtTokenService;

    private String token;

    @Before
    public void setUp() throws Exception {
        token = jwtTokenService.generateJwtToken("admin");
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void authenticationTest() throws Exception {
        JwtAuthenticationRequest request = new JwtAuthenticationRequest("admin", "password");

        mvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request)))
                .andExpect(status().is2xxSuccessful());
    }



    @Test(expected = AuthException.class)
    public void authenticationFailedTest() throws Exception {
        JwtAuthenticationRequest request = new JwtAuthenticationRequest("admin", "apassword");

        mvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request)));
    }
}
