package ara.security.jwt.resources;

import ara.security.jwt.model.Ville;
import ara.security.jwt.repository.VilleRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/api/ville")
public class VilleController {

    private final VilleRepository villeRepository;

    public VilleController(VilleRepository villeRepository) {
        this.villeRepository = villeRepository;
    }

    @GetMapping
    public ResponseEntity<Collection<Ville>> listAllVille() {
        return ResponseEntity.ok(villeRepository.findAll());
    }
}
