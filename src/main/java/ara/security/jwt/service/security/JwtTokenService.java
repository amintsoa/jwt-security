package ara.security.jwt.service.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Date;

@Service
public class JwtTokenService {

    private static final String TOKEN_PREFIX = "Bearer ";

    private final CustomUserDetailsService userDetailsService;

    @Value("${jwt.expiration}")
    private Long expiration;

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.header}")
    private String headerAuth;

    public JwtTokenService(CustomUserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    public String generateJwtToken(String username) {
        return generateJwtToken(username, expiration, secret);
    }

    public String generateJwtToken(String username, Long expiration, String secret) {
        ZonedDateTime createdDate = ZonedDateTime.now(ZoneOffset.UTC);
        ZonedDateTime expirationTime = createdDate.plus(expiration, ChronoUnit.MILLIS);

        return Jwts.builder()
                .setSubject(username)
                .setIssuedAt(Date.from(createdDate.toInstant()))
                .setExpiration(Date.from(expirationTime.toInstant()))
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }

    public UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(headerAuth);
        if (token == null)
            return null;
        String username = Jwts.parser().setSigningKey(secret)
                .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                .getBody()
                .getSubject();

        if (username != null) {
            return new UsernamePasswordAuthenticationToken(userDetailsService.loadUserByUsername(username), null, Collections.emptyList());
        }

        return null;
    }

    public void addAuthorizationHeader(HttpServletResponse response, String username) {
        String token = generateJwtToken(username);
        response.addHeader(headerAuth, TOKEN_PREFIX + token);
    }

    public boolean isAuthHeaderPresent(HttpServletRequest request) {
        String header = request.getHeader(headerAuth);

        return header != null && header.startsWith(TOKEN_PREFIX);
    }
}
